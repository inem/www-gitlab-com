---
layout: markdown_page
title: "Brand Guidelines"
---
# Welcome to the Brand Guidelines Handbook
{:.no_toc}

[Up one level to the Design Handbook](/handbook/marketing/corporate-marketing/design/)    

----

## On this page
{:.no_toc}

- TOC
{:toc}

----


## Introduction

Welcome to the Brand Guidelines handbook, here you will learn about the GitLab brand and its usage. To download the GitLab logo (in various formats and file types) check out our [Press page](https://about.gitlab.com/press/).

**Have ideas or suggestions?** Create an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/issues) issue tracker.

## The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [vision](https://about.gitlab.com/about/#vision) that everyone can contribute, our [values](https://about.gitlab.com/about/#values), and our [open source stewardship](https://about.gitlab.com/2016/01/11/being-a-good-open-source-steward/).

## Colors

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials.

#### Hex/RGB

![GitLab Hex/RGB Colors](/images/handbook/marketing/corporate-marketing/design/gitlab-hex-rgb-colors.png)

## Typography

The GitLab brand is paired with two typefaces. [Lato](https://fonts.google.com/specimen/Lato) for large display type and headlines, and [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro) for body copy; or main blocks of written content.

## [GitLab Product UX Guide](https://docs.gitlab.com/ce/development/ux_guide/)

The goal of this guide is to provide written standards, principles and in-depth information to design beautiful and effective GitLab features. This is a living document and will be updated and expanded as we iterate.

## [GitLab Product UX Design Pattern Library](https://brand.ai/git-lab/primary-brand/)

We've broken out the GitLab interface into a set of atomic pieces to form this design pattern library. This library allows us to see all of our patterns in one place and to build consistently across our entire product.

## Brand Police

Occasionally the [old GitLab logo](https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue. Thanks for contributing to our brand integrity!
